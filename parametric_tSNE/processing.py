
# coding: utf-8

# In[ ]:


import urllib.request 
import pandas as pd

from PIL import Image
import requests
from io import BytesIO
import numpy as np

# %matplotlib inline
import os
import random
# import cPickle as pickle
import numpy as np
from .core import Parametric_tSNE
import tensorflow as tf 

import keras
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K

from sklearn.decomposition import PCA

from sklearn.externals import joblib

def process_image(path):
    ret_path = path.split('\\')[-1]
    img = image.load_img(path, target_size=(224,224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    return ret_path, x


def download(i):
    df = pd.read_excel('./data/IG_Posts_Brands_prepared.xlsx')
    try:
        urllib.request.urlretrieve(df.thumbnail[i],"./data/img/"+str(df.post_id[i])+df.thumbnail[i][-4:])
    except Exception as e:
        print(e)


# In[ ]:


def download_list(id_list):
    df = pd.read_excel('./data/IG_Posts_Brands_prepared.xlsx')
    for i in id_list:
        try:
            urllib.request.urlretrieve(df.thumbnail[i],"./data/img/"+str(df.post_id[i])+df.thumbnail[i][-4:])
        except Exception as e:
            print(e)


# In[ ]:


def download_list_from_url(url_list):
    img_path = []
    ret=[]
    first = True
    for url in url_list:
        try :
            if len(url.split('/')[-1]) <=75 :
                response = requests.get(url)
                img = Image.open(BytesIO(response.content))
                img = img.resize((224,224), Image.ANTIALIAS)
                x = np.asarray(img)
                x = np.expand_dims(x, axis=0)
                ret.append(x)
                img_path.append(url.split('/')[-1])
        except Exception as e:
            print(e)
    ret = np.concatenate((ret),axis=0)
    
    print(ret.shape)
    return img_path, ret


# In[ ]:


def download_from_url(url):
    img_path = []
    ret=[]
    first = True
    try :
        if len(url.split('/')[-1]) <=75 :
            response = requests.get(url)
            img = Image.open(BytesIO(response.content))
            img = img.resize((224,224), Image.ANTIALIAS)
            x = np.asarray(img)
            ret = np.expand_dims(x, axis=0)
            img_path = url.split('/')[-1]
            return img_path, ret
    except Exception as e:
        print(e)
#     print(ret.shape)
    
def get_unique_img(previous_file, next_file):
    """
    Return the unique img list of images contained in previous set but not in next set.
    previous_file : hdf5 file, output of previous step
    next_file : hdf5_file, output of next step
    """
    with pd.HDFStore(next_file) as store:
        next_set = set(store.select(store.keys()[0], 'columns = ["img"]').img)
    
    with pd.HDFStore(previous_file) as store:
        previous_set = set(store.select(store.keys()[0], 'columns = ["img"]').img)
        
    return list(previous_set - next_set)


def extract_features(img,
                     X,
                     batch_size = 256,
                     feature_save_file = './raw_features.h5'):
    """
    Extract features from an image using resnet and saves in HDF5 files.
    img : list of img names
    X : matrix of shape (Nb of images, 224, 224, 3)
    batch size : for ResNet.
    feature_save_file : HDF5 file where ResNet features of the images are stored
    """
    K.clear_session()
    feat_extractor = keras.applications.resnet50.ResNet50(input_shape = (224,224,3),
                                                  weights='imagenet',
                                                  include_top=False,
                                                  pooling='avg')

    datagen = ImageDataGenerator(preprocessing_function=preprocess_input) #preprocessing_function=preprocess_input

    # generator = datagen.flow_from_directory(
    #         './data/img/small_sample/',
    #         target_size=(224, 224),
    #         batch_size=batch_size,
    #         class_mode=None,
    #         shuffle=False)

    ################ this generate images from a batch of url
    #     img, X = get_image(img_list)
    generator = datagen.flow(
                X,
    #             target_size=(224, 224),
                batch_size=batch_size,
                shuffle=False)
    #################

    features = feat_extractor.predict_generator(generator, steps=None) # fix step if you need to subsample
    
    K.clear_session()
    
    
    print(features.shape)
    df_features = pd.DataFrame(features,columns=["feat_"+str(i) for i in range((features.shape[1]))])
    df_features['img'] = img
    with pd.HDFStore(feature_save_file) as store:
        store.append('df_features', df_features, data_columns = ['img'], min_itemsize=75)

        
def apply_pca(img,
              feature_save_file = './raw_features.h5',
              pca_feature_save_file = './pca_features.h5',
              pca_model_path = './models/pca_trained_model.pkl',
              training = False):
    """
    Apply PCA on raw features and saves in HDF5 files.
    img : list of img names, should be in the feature_save_file.
    feature_save_file : HDF5 file where ResNet features of the images are stored
    pca_feature_save_file : HDF5 file where PCA features of the images are stored
    pca_model_path : path to the pca model file (pickle)
    training : Boolean : if True, trains the pca model and saves it.
    """
    with pd.HDFStore(feature_save_file) as store:
        df = store.select('df_features', where = "img in img")
        
    img = df['img'].as_matrix()
    df.drop(['img'],inplace=True,axis=1)
    features = df.as_matrix()
    
    # apply PCA
    if training:
        pca = PCA(n_components = 200)
        pca.fit(features)
        joblib.dump(pca, pca_model_path) 
    else:
        pca = joblib.load(pca_model_path)
        
    pca_features = pca.transform(features)
    print(pca_features.shape)
    df_pca_features = pd.DataFrame(pca_features,columns=["pca_feat_"+str(i) for i in range((pca_features.shape[1]))])
    df_pca_features['img'] = img
    with pd.HDFStore(pca_feature_save_file) as store:
        store.append('df_pca_features', df_pca_features, data_columns = ['img'], min_itemsize=75)


# In[ ]:


def ptSNE(img,
          pca_feature_save_file = './pca_features.h5',
          ptSNE_model_path = './models/ptSNE_trained_model.h5',
          tSNE_feature_save_file = './tSNE_features.h5',
          do_pretrain=True,
          training = False):
    """
    Load PCA features of an image, transform it using parametric t-SNE and save it as HDF5.
    img : list of img names, should be in the pca_feature_save_file.
    pca_feature_save_file : HDF5 file where PCA features of the images are stored
    ptSNE_model_path : path to the ptSNE model file (HDF5)
    tSNE_feature_save_file : HDF5 file to save transformed images
    training : Boolean : if True, trains the ptSNE model and saves it.
    """
    K.clear_session()

    with pd.HDFStore(pca_feature_save_file) as store:
    #     for df in store.select('df_pca_features', where = "img in img", chunksize=chunksize):
        df = store.select('df_pca_features', where = "img in img")

    # load images    
    img = df['img'].as_matrix()
    df.drop(['img'],inplace=True,axis=1)
    pca_features = df.as_matrix()

    print(pca_features.shape, img.shape)
    high_dims = pca_features.shape[1]
    num_outputs = 2 # number of dimensions for ptSNE output (default = 2)
    perplexity = 10

    #create model
    with tf.Session() as sess:
        ptSNE_model = Parametric_tSNE(high_dims, num_outputs, perplexity, do_pretrain = do_pretrain, training = training)

        # load or fit model
        if training :
            ptSNE_model.fit(pca_features)
            ptSNE_model.save_model(ptSNE_model_path)
        else:
            ptSNE_model.restore_model(ptSNE_model_path)

        # transform images
        output_res = ptSNE_model.transform(pca_features)
#         ptSNE_model.clear_session()
    K.clear_session()
    
    df_tSNE_features = pd.DataFrame(output_res,columns=["tx","ty"])
    df_tSNE_features['img'] = img

    with pd.HDFStore(tSNE_feature_save_file) as store:
        store.append('df_tSNE_features', df_tSNE_features, data_columns = ['img'], min_itemsize=75)


# In[ ]:


def visualize(img,
              tSNE_feature_save_file = './tSNE_features.h5',
              save_img_path = "./ptSNE_img.png",
              img_path = './data/img/small_sample/file/',
              width = 5000,
              height = 5000,
              max_dim = 25):
    
    import matplotlib.pyplot as plt
    with pd.HDFStore(tSNE_feature_save_file) as store:
        df = store.select('df_tSNE_features', where = "img in img")

    tx = df['tx']
    ty = df['ty']
    img = df['img']

    min_x =np.min(tx)
    max_x = np.max(tx)
    min_y = np.min(ty)
    max_y = np.max(ty)
    tx = (tx-min_x) / (max_x - min_x)
    ty = (ty-min_y) / (max_y - min_x)


#     t0 = time.time()
    full_image = Image.new('RGBA', (width, height))
    i = 0
    for img_, x, y in zip(img[:], tx[:], ty[:]):
        if i%int(len(tx[:])/10.)==0:
            print('img {}/{}'.format(i,len(tx[:])))
        i+=1
        tile = Image.open(img_path + img_)
        rs = max(1, tile.width/max_dim, tile.height/max_dim)
        tile = tile.resize((int(tile.width/rs), int(tile.height/rs)), Image.ANTIALIAS)
        full_image.paste(tile, (int((width-max_dim)*1*x), int((height-max_dim)*1*y)), mask=tile.convert('RGBA'))

#     print("total time : {}".format(time.time()-t0))
    plt.figure(figsize = (16,12))
#     plt.imshow(full_image)
    full_image.save(save_img_path)

