from setuptools import setup

setup(name='parametric_tSNE',
      version='1.0',
      description='parametric_tSNE',
      author='Packaged by Corentin Martel, based on https://github.com/jsilter/parametric_tsne',
      author_email='corentin.martel@corelyo.com',
      license='MIT',
      packages=['parametric_tSNE'],
      include_package_data = True,
      zip_safe=False)

